import Api from './Api'
import Cookie from 'js-cookie'
export default {
   getCookie(){
      let token = Cookie.getCookie('XSRF-TOKEN');
       if (token){
          return new Promise(resove => {
             resolve(token)
          })
       }
      return Api.get('/csrf-cookie')
   }
};