import { createWebHistory, createRouter } from "vue-router"
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import PageNotFound from '../views/PageNotFound.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Dashboard from '../views/Dashboard.vue'


const routes = [
    {
        path: '/:pathMatch(.*)*',
        component: PageNotFound,
        name: 'Found',
        meta: {
            title: 'Page Not Found'
        }
    },
    {
        path: '/',
        component: Home,
        name: 'Home',
        meta: {
            title: 'Home Page'
        }
    },
    {
        path: '/about',
        component: About,
        name: 'About',
        meta: {
            title: 'About Page'
        }
    },
    {
        path: '/login',
        component: Login,
        name: 'Login',
        meta: {
            title: 'Login',
            guestOnly: true,
        }
    },
    {
        path: '/register',
        component: Register,
        name: 'Register',
        meta: {
            title: 'Register',
            guestOnly: true,
        }
    },
    {
        path: '/dashboard',
        component: Dashboard,
        name: 'Dashboard',
        meta: {
            title: 'Dashboard',
            authOnly: true,
        }
    },

]

const router = createRouter(
    {
        history: createWebHistory(),
        routes,
    });


function isLoggedIn() {
    return localStorage.getItem("auth");
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.authOnly)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!isLoggedIn()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.guestOnly)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (isLoggedIn()) {
            next({
                path: '/dashboard',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router;
