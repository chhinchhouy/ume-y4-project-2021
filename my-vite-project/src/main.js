import { createApp } from 'vue'
import mitt from 'mitt';
const emitter = mitt();
import router from './router/index'
import App from './App.vue'
import './index.css'
import axios from 'axios';

const app = createApp(App);

axios.defaults.withCredentials = true;
app.config.globalProperties.emitter = emitter;

app.use(router).mount('#app')
